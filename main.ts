import { Elysia, redirect } from 'elysia';
import { red, gray, black, blueBright, greenBright, bgBlueBright } from 'ansi-colors';

// Login handler
class LoginHandler {

  private static readonly VALID_AUTH = 'admin:admin';
  static readonly REALM = 'LoginExample';

  private username = '';
  private password = '';

  private decodeAuthorization(authorization: string) {
    const authDataBase64 = authorization.split(' ')[1]!;
    const authData = atob(authDataBase64);
    const [username, password] = authData.split(':');
    this.username = username!;
    this.password = password!;
  }
  handle(authorization: string): Response {
    console.info(bgBlueBright(black('-> Login attempt! <-')));
    console.info(gray(`\`Authorization: ${authorization}\``));

    this.decodeAuthorization(authorization);
    console.info(`Username: ${blueBright(this.username)}`);
    console.info(`Password: ${blueBright(this.password)}`);

    if (this.getCleartextAuth() !== LoginHandler.VALID_AUTH) {
      console.info(red('Authentication credentials didn\'t match.\n\n\n'));
      return redirect('/401');
    }
    console.info(greenBright('Authentication credentials are valid!\n\n\n'));
    return redirect('/welcome');
  }

  getCleartextAuth(): string {
    return `${this.username}:${this.password}`;
  }
  getUsername(): string {
    return this.username;
  }
  getPassword(): string {
    return this.password;
  }

  static readonly INSTANCE = new LoginHandler;
}

// Server definitions
const routes = new Elysia()
    .get('/', () => {
      const body = Bun.file('./frontend/index.html');
      return new Response(body);
    })
    .get('/login', ({ headers }) => {
      const authorization = headers['authorization'];
      if (authorization != undefined) {
        return LoginHandler.INSTANCE.handle(authorization);
      }
      return new Response(undefined, {
        status: 401,
        statusText: 'Unauthorized',
        headers: [
          ['WWW-Authenticate', `Basic realm="${LoginHandler.REALM}"`],
        ],
      });
    })
    .get('/welcome', () => {
      const body = Bun.file('./frontend/welcome.html');
      return new Response(body);
    })
    .get('/styles.css', () => {
      const body = Bun.file('./frontend/styles.css');
      return new Response(body);
    });

const lifeCycleHooks = new Elysia()
    .onStart(({ server }) => {
      console.log(`Listening on ${server!.url}`);
    })
    .onRequest(({ request: { method, url, headers } }) => {
      console.log(`${method} ${url}`);
      headers.forEach((value, name) => {
        console.log(`${name}: ${value}`);
      });
      console.log('\n');
    });

const errorHandling = new Elysia()
    .get('/401', () => {
      const body = Bun.file('./frontend/error_pages/401.html');
      return new Response(body, {
        status: 401,
        statusText: 'Unauthorized',
      });
    })
    .get('/404', () => {
      const body = Bun.file('./frontend/error_pages/404.html');
      return new Response(body, {
        status: 404,
        statusText: 'Not Found',
      });
    })
    .onError(({ error, redirect }) => {
      switch (error.name) {
        case 'NOT_FOUND':
          return redirect('/404', 404);
        case 'VALIDATION':
          return redirect('/401', 401);
        default:
          return error;
      }
    });

// Initialization
new Elysia()
    .use(routes)
    .use(errorHandling)
    .use(lifeCycleHooks)
    .listen(8080);
