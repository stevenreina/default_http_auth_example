# default_http_auth_example

### How to use

1. Download packages
   ```bash
   bun install
   ```
2. Run the server
   ```bash
   bun run start
   ```
3. Lastly, open `http://localhost:8080/` in your browser and attempt to log in. The only valid credentials are:

   - Username: `admin`
   - Password: `admin`

   Every request will be printed out on the server's stdout, and every attempt to log in will be printed, in pretty format. **The password will be displayed as cleartext!**

### Frontend screenshot

The frontend design is as simple as it can get, although not completely uncared-of.

![Frontend screenshot](./frontend_screenshot.png)

### Output example

Example of an HTTP GET request on `/login`, plus the aditional info it displays when attempting to authenticate:

![Output example](./output_example.png)
